# This script launches a Flatcar Container Linux VM  within VMWare Fusion
# Once the VM has launced import into VMWare Fusion: File >> Scan for Virtual Machines
# Download the ova with `curl -LO https://stable.release.flatcar-linux.net/amd64-usr/current/flatcar_production_vmware_ova.ova``
 
# Create the ignition JSON from Butane yaml
docker run --interactive --rm --security-opt label=disable --volume ${PWD}:/pwd --workdir /pwd quay.io/coreos/butane:latest --pretty --strict --files-dir=. config.bu > transpiled_config.ign

# Launch image
CONFIG_ENCODING='base64'
CONFIG_ENCODED=$(cat transpiled_config.ign | base64 -i -)
VM_NAME='Flatcar Stable'
FLATCAR_OVA='flatcar_production_vmware_ova.ova'
LIBRARY="$HOME/Virtual Machines.localized"
/Applications/VMware\ OVF\ Tool/ovftool \
  --powerOffTarget \
  --name="${VM_NAME}" \
  --allowExtraConfig \
  --extraConfig:guestinfo.ignition.config.data.encoding="${CONFIG_ENCODING}" \
  --extraConfig:guestinfo.ignition.config.data="${CONFIG_ENCODED}" \
  "${FLATCAR_OVA}" "${LIBRARY}"